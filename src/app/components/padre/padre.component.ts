import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css'],
})
export class PadreComponent implements OnInit {
  //De que tipo es el mensaje que recibira el padre
  mensajePadre!: string;
  mensajePadre2!: string;
  frasePadre!: string;
  numeroPadre!: number;
  booleanPadre!: boolean;
  constructor() {}

  ngOnInit(): void {}

  recibirMensaje($event: string): void {
    this.mensajePadre = $event;
  }

  recibirMensaje2($event: string): void {
    this.mensajePadre2 = $event;
  }

  recibirFrase($event: string): void {
    this.frasePadre = $event;
  }

  recibirNumero($event: number): void {
    this.numeroPadre = $event;
  }

  recibirBoolean($event: boolean): void {
    this.booleanPadre = $event;
  }
}
