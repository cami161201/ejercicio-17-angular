import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styleUrls: ['./hijo.component.css'],
})
export class HijoComponent implements OnInit {
  mensaje = 'Mensaje del hijo al padre';
  mensaje2 =
    'Los obstáculos son esas cosas atemorizantes que ves cuando apartas los ojos de tu meta - Henry Ford';
  mensaje3 =
    '¿Por qué contentarnos con vivir a rastras cuando sentimos el anhelo de volar? - Hellen Keller';
  numero = 161201;
  verdad = true;

  @Output() mensajeHijo = new EventEmitter<string>();
  @Output() mensajeHijo2 = new EventEmitter<string>();
  @Output() fraseHijo3 = new EventEmitter<string>();
  @Output() numeroHijo = new EventEmitter<number>();
  @Output() booleanHijo = new EventEmitter<boolean>();
  constructor() {}

  ngOnInit(): void {
    this.mensajeHijo.emit(this.mensaje);
    this.mensajeHijo2.emit(this.mensaje2);
    this.fraseHijo3.emit(this.mensaje3);
    this.numeroHijo.emit(this.numero);
    this.booleanHijo.emit(this.verdad);
  }
}
